# pet-gps-finder

## Summary and introduction

> <span style="color:orange">&#9888;</span>**Warning**: This application is intended to be a portfolio and learning project. It's not intended to be used as a functional product or part of one, thus the functionality is limited and the stability is not guaranteed. this proyect is currently unfinished and in a very early stage of development.

This project it's a pet finder application prototype and proof of concept developed with a DDD micro services approach and event-driven architecture implemented over reactive services using Spring webflux and RabbitMQ as broker for real-time messaging, this approach aims to provide scalability for hundred of thousand of concurrent clients and IoT devices (here called "producers"). It consists of 2 modules so far, with a third module to be considered in the future.


* The first module is compose by all the backend microservices, including databases, crud services and a consumer for the message broker system.
* The second module consist of a monolithic producer service to deploy to the edge devices (in this case a raspberry pi zero 2W with a GPS device through a serial interface).
* The third module may consist of a very simple client-side application to interact with the whole system. This is still something I am thinking about, but most probable it will be a mobile application for android usin Kotlin; I am also thinking about a multi-platform application developed with Flutter/Dart but I am still not sure.

### Requirements

- Springboot framework 3.2.x
- Spring webflux.
- R2DBC for Postgres
- Mongo reactive
- Java 21
- MongoDB 7
- KeyCloak
- Postgres 15
- RabbitMQ 3.x
- Docker 26
- Docker compose 1.27.x

### Environment

- **Development**: Fedora 39.
- **Backend**: Fedora 39.
- **Edge devices**: Raspberry pi zero 2w with Raspbian lite.

This project it's a pet finder application prototype and proof of concept developed with a DDD micro services approach and event-driven architecture implemented over reactive services. It consists of 2 modules so far, with a third module to be considered in the future.

- The first module is compose by all the backend microservices, including databases, crud services and a consumer for the message broker system.
- The second module consist of a monolithic producer service to deploy to the ede devices (in this case a raspberry pi zero 2W with a GPS device and a serial interface).
- The third modulo may consiste of a simple client-side application to interact with the whole system. This is still something I am thinking about, but most probable it will be a mobile application for android. I am also thinking about a multi-platform application developed with Flutter/Dart, but I am still not sure.

## TO-DO

- **Develop the IoT services (in this moment it's a simple dumb producer for testing the backend services).**
- Automatize the deployment with docker-compose scripts for all services.
- Implement security service (Keycloak).
- **To Develop client-side mobile application (if ever created).**
- To create unitary test for each micro-service.
- Add some deployment isntructions.

> <span style="color:blue">&#9888;</span> **Bold To-Do task may be implemented as standalone portfolio applications, links will be provided when they are ready.**

## Architecture

### microservices

- **Producer service:** IoT service responsable for collecting the gps data and sending the data through the broker subsystem.
- **Consumer service:** Backend service that consumes the data from the IoT edge devices and saves the events to a mongoDB database.
- **Events crud service:** Backend service to manage saved events.
- **Main crud service:** Backend service to manage users, pets and device related data.
- **RabbitMQ:** Broker system used mainly to communicate consumer and producer services.
- **MongoDB:** Database used to store events.
- **Postgres:** Database used to store users, pets and device data.

### Architecture diagram

![alt text](assets/architectureDiagram.png)

## Interacting with the Services

One can play with the microservices using the OpenAPi swagger-ui interface. This interface it's accesible through the Spring cloud API-Gateway and the Eureka Discovery service. Please note that you cannot create events because these ae supposed to be created only byendge devices through the event producer-consumer and these communicate with each other only using RabbitMQ, so there is no controller for manual handling of events. 

To access this interface you only need to go to <http://localhost:8003/swagger> once  and select the desired service from the dropdown menu displayed.

![](assets/swagger.png)

## Deployment process.

### Deployment order

1. Eureka
2. Gateway
3. MongoDB
4. Postgres
5. RabbitMQ
6. The other services can be deployed in any order
