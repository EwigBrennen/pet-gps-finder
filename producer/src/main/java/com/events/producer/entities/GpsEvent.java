package com.events.producer.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public record GpsEvent(
        String latitude,
        String longitude,
        String altitude,
        String serial
) {

}
