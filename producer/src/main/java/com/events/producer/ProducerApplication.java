package com.events.producer;

import com.events.producer.entities.GpsEvent;
import com.events.producer.service.ProducerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@SpringBootApplication
public class ProducerApplication implements CommandLineRunner {
	List<String> latitudList = List.of(
			"4.5985", "6.2442", "3.4372", "11.0041", "7.8891",
			"10.4685", "1.2136", "4.5709", "4.1409", "8.5910",
			"2.9355", "10.3966", "7.8215", "4.6328", "3.7979",
			"11.0133", "7.0975", "1.1986", "4.6811", "4.3309"
	);
	List<String> longitudList = List.of(
			"-74.0758", "-75.5736", "-76.5225", "-74.8069", "-72.4993",
			"-73.2518", "-77.2795", "-75.6443", "-73.6350", "-76.8749",
			"-75.2883", "-75.4780", "-72.4388", "-74.0657", "-76.3526",
			"-74.8360", "-73.1235", "-77.3554", "-75.4893", "-73.5661"
	);
	List<String> altitudList = List.of(
			"500", "300", "700", "200", "400",
			"600", "100", "250", "350", "450",
			"550", "150", "650", "750", "250",
			"350", "450", "550", "150", "250"
	);
	List<String> serialList = List.of("857468421","32148752135","3625487514");


	@Autowired
	ProducerService producerService;

	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		try(ExecutorService executorService =  Executors.newVirtualThreadPerTaskExecutor()){
			Future<?> task = executorService.submit(this::createMessage);

		}



	}

	private void createMessage() {
		String latitude;
		String longitude;
		String altitude;
		String serial;
		Random random = new Random();
		for (int i = 0; i < 5; i++) {

			serial = serialList.get(random.nextInt(0, serialList.size()));
			latitude = latitudList.get(random.nextInt(0, latitudList.size()));
			longitude = longitudList.get(random.nextInt(0, longitudList.size()));
			altitude = altitudList.get(random.nextInt(0, longitudList.size()));
			try {
				producerService.sendMessage(
						new GpsEvent(latitude, longitude, altitude, serial)
				);
			} catch (JsonProcessingException e) {
				throw new RuntimeException(e);
			}
			try {
				Thread.sleep(Duration.ofSeconds(1));
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

		}
	}
}
