package com.events.producer.service;

import com.events.producer.entities.GpsEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    public void sendMessage(GpsEvent gpsEvent) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(gpsEvent);
        System.out.println(json);
        rabbitTemplate.convertAndSend("q.gps",json);
    }
}
