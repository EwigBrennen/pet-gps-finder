package com.gps_pet_finder.curd_events_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication

public class CrudEventsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudEventsServiceApplication.class, args);
	}

}
