package com.gps_pet_finder.curd_events_service.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
@Document(collection = "gpsEvent")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GpsEvent{
    @Id
    private String id;
    private String latitude;
    private String longitude;
    private String altitude;
    private LocalDate localDate;
    private String serial;

}
