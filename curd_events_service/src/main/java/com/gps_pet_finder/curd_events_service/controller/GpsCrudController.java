package com.gps_pet_finder.curd_events_service.controller;
import com.gps_pet_finder.curd_events_service.entities.GpsEvent;
import com.gps_pet_finder.curd_events_service.service.interfaces.GpsEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import java.time.LocalDate;

@RestController
@RequestMapping("/events")
@CrossOrigin
public class GpsCrudController {
    @Autowired
    GpsEventService gpsEventService;

    @GetMapping("/get-by/all")
    public Flux<GpsEvent> getAll(){
        return gpsEventService.getAllEvents()
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }
    @GetMapping("/get-by/serial")
    public  Flux<GpsEvent> getBySerial(@RequestParam String serial) {
        return gpsEventService.getGpsEventsBySerial(serial)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")));
    }
    @GetMapping("/get-by/serial-and-date")
    public Flux<GpsEvent> getBySerialAndDate(@RequestParam String serial, LocalDate date){
        return gpsEventService.getGpsEventsBySerialAndDate(serial,date)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")));
    }
    @GetMapping("/get-by/serial-and-dabe-between")
    public Flux<GpsEvent> getBySerialAndDate(@RequestParam String serial, LocalDate startDate, LocalDate endDate){
        return gpsEventService.getGpsEventsBySerialAndDate(serial,startDate,endDate)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")));
    }

    @DeleteMapping("/delete-by/all")
    public  Mono<Void> deleteAllEvents(){
        return gpsEventService.deleteAllGpsEvents();
    }
    @DeleteMapping("/delete-by/serial")
    public Mono<Void> deleteGpsBySerial(@RequestParam String serial){
        return gpsEventService.getGpsEventsBySerial(serial)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")))
                .then(gpsEventService.deleteEventsBySerial(serial));


    }
    @DeleteMapping("/delete-by/all-by-date")
    public Mono<Void> deleteAllByDate(@RequestParam LocalDate date){
        return gpsEventService.deleteAllEventByDate(date)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")))
                .then(gpsEventService.deleteAllEventByDate(date));
    }
    @DeleteMapping("/delete-by/all-by-date-between")
    public Mono<Void> deleteAllByDateBetween(@RequestParam LocalDate startDate,@RequestParam LocalDate endDate){
        return gpsEventService.deleteAllEventByDate(startDate, endDate)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")))
                .then(gpsEventService.deleteAllEventByDate(startDate, endDate));
    }

    @DeleteMapping("/delete-by/serial-and-date")
    public Mono<Void> deleteBySerialAndDate(@RequestParam String serial,@RequestParam LocalDate date){
        return gpsEventService.deleteEventsBySerialAndDate(serial, date)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")))
                .then(gpsEventService.deleteEventsBySerialAndDate(serial,date));
    }
    @DeleteMapping("/delete-by/serial-and-date-between")
    public Mono<Void> deleteBySerialAndDateBetween(@RequestParam String serial,@RequestParam LocalDate startDate,LocalDate endDate){
        return gpsEventService.deleteEventsBySerialAndDate(serial, startDate,endDate)
                .switchIfEmpty(Mono.error(new NotFoundException("Event with the given parameter doesn't exist")))
                .then(gpsEventService.deleteEventsBySerialAndDate(serial,startDate,endDate));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static class NotFoundException extends RuntimeException {
        public NotFoundException(String message) {
            super(message);
        }
    }
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNotFoundException(NotFoundException ex) {
        return ex.getMessage();
    }

}
