package com.gps_pet_finder.curd_events_service.service.interfaces;

import com.gps_pet_finder.curd_events_service.entities.GpsEvent;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface GpsEventService {

    Flux<GpsEvent> getAllEvents();
    Flux<GpsEvent> getGpsEventsBySerial(String serial);
    Flux<GpsEvent> getGpsEventsBySerialAndDate(String serial, LocalDate startingDate, LocalDate EndingDate);
    Flux<GpsEvent> getGpsEventsBySerialAndDate(String serial, LocalDate date);
    Mono<Void> deleteAllGpsEvents();
    Mono<Void> deleteEventsBySerial(String serial);
    Mono<Void> deleteAllEventByDate(LocalDate startDate, LocalDate endDate);
    Mono<Void> deleteAllEventByDate(LocalDate startDate);
    Mono<Void> deleteEventsBySerialAndDate(String serial, LocalDate date);
    Mono<Void> deleteEventsBySerialAndDate(String serial, LocalDate startDate, LocalDate endDate);



}
