package com.gps_pet_finder.curd_events_service.service;

import com.gps_pet_finder.curd_events_service.entities.GpsEvent;
import com.gps_pet_finder.curd_events_service.repository.GpsEventRepository;
import com.gps_pet_finder.curd_events_service.service.interfaces.GpsEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
@Service
public class GpsEventServiceImp implements GpsEventService {
    @Autowired
    GpsEventRepository gpsEventRepository;
    @Override
    public Flux<GpsEvent> getAllEvents() {
        return gpsEventRepository.findAll();
    }

    @Override
    public Flux<GpsEvent> getGpsEventsBySerial(String serial) {
        return gpsEventRepository.findBySerial(serial);
    }

    @Override
    public Flux<GpsEvent> getGpsEventsBySerialAndDate(String serial, LocalDate startingDate, LocalDate endingDate) {
        return gpsEventRepository.findBySerialAndLocalDateBetween(serial,startingDate, endingDate);
    }

    @Override
    public Flux<GpsEvent> getGpsEventsBySerialAndDate(String serial, LocalDate date) {
        return gpsEventRepository.findBySerialAndLocalDate(serial,date);
    }
    @Override
    public Mono<Void> deleteAllGpsEvents(){
        return gpsEventRepository.deleteAll();
    }

    @Override
    public Mono<Void> deleteEventsBySerial(String serial) {
        return gpsEventRepository.deleteBySerial(serial);
    }


    @Override
    public Mono<Void> deleteAllEventByDate(LocalDate startDate, LocalDate endDate) {
        return gpsEventRepository.deleteAllByLocalDateBetween(startDate, endDate);
    }

    @Override
    public Mono<Void> deleteAllEventByDate(LocalDate date) {
        return gpsEventRepository.deleteAllByLocalDate(date);
    }

    @Override
    public Mono<Void> deleteEventsBySerialAndDate(String serial, LocalDate date) {
        return gpsEventRepository.deleteBySerialAndLocalDate(serial, date);
    }

    @Override
    public Mono<Void> deleteEventsBySerialAndDate(String serial, LocalDate startDate, LocalDate endDate) {
        return gpsEventRepository.deleteBySerialAndLocalDateBetween(serial, startDate, endDate);
    }



}
