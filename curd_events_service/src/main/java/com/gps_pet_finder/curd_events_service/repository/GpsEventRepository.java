package com.gps_pet_finder.curd_events_service.repository;

import com.gps_pet_finder.curd_events_service.entities.GpsEvent;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Repository
public interface GpsEventRepository extends ReactiveMongoRepository<GpsEvent,String> {
    Flux<GpsEvent> findBySerial(String serial);
    Flux<GpsEvent> findBySerialAndLocalDateBetween(String serial, LocalDate startDate, LocalDate endDate);
    Flux<GpsEvent> findBySerialAndLocalDate(String serial, LocalDate startDate);
    Mono<Void> deleteAll();
    Mono<Void> deleteBySerial(String serial);
    Mono<Void> deleteAllByLocalDate(LocalDate date);
    Mono<Void> deleteBySerialAndLocalDate(String serial, LocalDate date);
    Mono<Void> deleteBySerialAndLocalDateBetween(String serial, LocalDate startDate, LocalDate endDate);
    Mono<Void> deleteAllByLocalDateBetween(LocalDate startDate, LocalDate endDate);
}
