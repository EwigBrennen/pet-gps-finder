package com.real_time_pet_finder.crud_service.repositories;

import com.real_time_pet_finder.crud_service.entities.DTOs.pet.PetDTO;
import com.real_time_pet_finder.crud_service.entities.GPS;
import com.real_time_pet_finder.crud_service.entities.Pet;

import org.springframework.data.r2dbc.repository.R2dbcRepository;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Repository
public interface PetRepository extends R2dbcRepository<Pet, Long> {
    Flux<PetDTO> findByClientId(long clientId);

}
