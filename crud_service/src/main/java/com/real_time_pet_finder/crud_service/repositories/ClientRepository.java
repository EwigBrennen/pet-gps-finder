package com.real_time_pet_finder.crud_service.repositories;

import com.real_time_pet_finder.crud_service.entities.Client;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface ClientRepository extends R2dbcRepository<Client, Long> {
}
