package com.real_time_pet_finder.crud_service.repositories;

import com.real_time_pet_finder.crud_service.entities.GPS;

import org.springframework.data.r2dbc.repository.R2dbcRepository;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface GPSRepository extends R2dbcRepository<GPS, Long> {
    Flux<GPS> findByClientId(long clientId);
}
