package com.real_time_pet_finder.crud_service.services.interfaces;

import com.real_time_pet_finder.crud_service.entities.DTOs.pet.PetDTO;
import com.real_time_pet_finder.crud_service.entities.GPS;
import reactor.core.publisher.Flux;

public interface PetService {
    Flux<PetDTO> getAllPetsByClientId(long clientId);
}
