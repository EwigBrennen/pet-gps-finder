package com.real_time_pet_finder.crud_service.services;

import com.real_time_pet_finder.crud_service.entities.Client;
import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientDTOResponseWithPets;
import com.real_time_pet_finder.crud_service.repositories.ClientRepository;
import com.real_time_pet_finder.crud_service.repositories.PetRepository;
import com.real_time_pet_finder.crud_service.services.interfaces.BasicCrud;
import com.real_time_pet_finder.crud_service.services.interfaces.ClientService;
import com.real_time_pet_finder.crud_service.util.Entity2DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ClientServiceImp implements BasicCrud<Client>, ClientService {
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    PetRepository petRepository;
    @Override
    public Mono<Client> getById(long id) {
        return clientRepository.findById(id);
    }

    @Override
    public Flux<Client> getAll() {
        return clientRepository.findAll();
    }

    @Override
    public Mono<Client> create(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public Mono<Void> removeById(long id) {
            return clientRepository.deleteById(id);
    }

    @Override
    public Mono<Client> update(long id, Client client) {
        return clientRepository.findById(id)
                .flatMap(existingClient ->{
                    existingClient.setId(client.getId());
                    existingClient.setCode(client.getCode());
                    existingClient.setName(client.getName());
                    existingClient.setSurname(client.getSurname());
                    return clientRepository.save(existingClient);
                });
    }


    @Override
    public Mono<ClientDTOResponseWithPets> getClientithPets(long clientId) {
        return clientRepository.findById(clientId)
                .flatMap(client -> petRepository.findByClientId(clientId)
                        .collectList()
                        .map(pets ->{
                            var clientDto =new ClientDTOResponseWithPets(
                                    client.getId(),
                                    client.getName(),
                                    client.getEmail(),
                                    client.getSurname(),
                                    client.getCode(),
                                    client.getRole(),
                                    pets
                            );
                            return clientDto;
                        }));
    }
}
