package com.real_time_pet_finder.crud_service.services;

import com.real_time_pet_finder.crud_service.entities.GPS;
import com.real_time_pet_finder.crud_service.repositories.GPSRepository;
import com.real_time_pet_finder.crud_service.services.interfaces.BasicCrud;
import com.real_time_pet_finder.crud_service.services.interfaces.GPSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Service
public class GPSServiceImp implements BasicCrud<GPS>, GPSService {
    @Autowired
    GPSRepository gpsRepository;

    @Override
    public Mono<GPS> getById(long id) {
        return gpsRepository.findById(id);
    }

    @Override
    public Flux<GPS> getAll() {
        return gpsRepository.findAll();
    }

    @Override
    public Mono<GPS> create(GPS gps) {
        return gpsRepository.save(gps);
    }

    @Override
    public Mono<Void> removeById(long id) {
        return gpsRepository.deleteById(id);
    }

    @Override
    public Mono<GPS> update(long id, GPS gps) {
        return gpsRepository.findById(id)
                .flatMap(existingGPS ->{
                    existingGPS.setId(gps.getId());
                    existingGPS.setClientId(gps.getClientId());
                    existingGPS.setPetId(gps.getPetId());
                    existingGPS.setModel(gps.getModel());
                    existingGPS.setUuid(gps.getUuid());
                    return gpsRepository.save(existingGPS);
                });
    }


    @Override
    public Flux<GPS> getAllDevicesByClientId(long clientId) {
        return gpsRepository.findByClientId(clientId);
    }
}
