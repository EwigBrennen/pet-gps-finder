package com.real_time_pet_finder.crud_service.services.interfaces;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BasicCrud <T>{
        Mono<T> getById(long id);
        Flux<T> getAll();
        Mono<T>create(T t);
        Mono<Void> removeById(long id);
        Mono<T> update(long id, T t);



}
