package com.real_time_pet_finder.crud_service.services.interfaces;

import com.real_time_pet_finder.crud_service.entities.GPS;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface GPSService {
    Flux<GPS> getAllDevicesByClientId(long clientId);
}
