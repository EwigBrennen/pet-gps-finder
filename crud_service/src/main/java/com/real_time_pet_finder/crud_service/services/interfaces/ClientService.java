package com.real_time_pet_finder.crud_service.services.interfaces;

import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientDTOResponseWithPets;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ClientService {
    Mono<ClientDTOResponseWithPets> getClientithPets(long clientId);
}
