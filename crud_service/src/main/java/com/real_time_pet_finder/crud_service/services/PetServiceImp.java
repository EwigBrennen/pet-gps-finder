package com.real_time_pet_finder.crud_service.services;

import com.real_time_pet_finder.crud_service.entities.DTOs.pet.PetDTO;
import com.real_time_pet_finder.crud_service.entities.GPS;
import com.real_time_pet_finder.crud_service.entities.Pet;
import com.real_time_pet_finder.crud_service.repositories.PetRepository;
import com.real_time_pet_finder.crud_service.services.interfaces.BasicCrud;
import com.real_time_pet_finder.crud_service.services.interfaces.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Service
public class PetServiceImp implements BasicCrud<Pet>, PetService {
    @Autowired
    PetRepository petRepository;


    @Override
    public Mono<Pet> getById(long id) {
        return petRepository.findById(id);
    }

    @Override
    public Flux<Pet> getAll() {
        return petRepository.findAll();
    }

    @Override
    public Mono<Pet> create(Pet pet) {
        return petRepository.save(pet);
    }

    @Override
    public Mono<Void> removeById(long id) {
        return petRepository.deleteById(id);
    }

    @Override
    public Mono<Pet> update(long id, Pet pet) {
        return petRepository.findById(id)
                .flatMap(existingPet ->{
                    existingPet.setId(pet.getId());
                    existingPet.setClientId(pet.getClientId());
                    existingPet.setDeviceId(pet.getDeviceId());
                    existingPet.setSpecies(pet.getSpecies());
                    return petRepository.save(existingPet);
                });
    }


    @Override
    public Flux<PetDTO> getAllPetsByClientId(long clientId) {
        return petRepository.findByClientId(clientId);
    }
}
