package com.real_time_pet_finder.crud_service.entities.DTOs.client;

public record ClientResponseUserDTO(
        long id,
        String name,
        String email,
        String surname,
        String code
) {
}
