package com.real_time_pet_finder.crud_service.entities.DTOs.client;

public record ClientRequestUserDTO(
        long id,
        String name,
        String email,
        String password,
        String surname,
        String code
) {
}
