package com.real_time_pet_finder.crud_service.entities;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.databind.JsonNode;
import com.real_time_pet_finder.crud_service.entities.enums.Species;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "pets")
public class Pet {
    @Id
    private long id;
    private Species species;
    @Column("client_id")
    private long clientId;
    @Column("device_id")
    private long deviceId;

}
