package com.real_time_pet_finder.crud_service.entities.DTOs.gps;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

import java.util.UUID;

public record GPSResponseDTO(
         long id,
         long petId,
         long clientId,
         String model,
         UUID uuid
) {
}
