package com.real_time_pet_finder.crud_service.entities.DTOs.pet;

import com.fasterxml.jackson.databind.JsonNode;
import com.real_time_pet_finder.crud_service.entities.enums.Species;
import org.springframework.data.relational.core.mapping.Column;

public record PetDTO (
     long id,
     Species species,
     long clientId,
     long deviceId

){}
