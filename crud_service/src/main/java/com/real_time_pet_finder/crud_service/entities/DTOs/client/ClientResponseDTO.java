package com.real_time_pet_finder.crud_service.entities.DTOs.client;

import com.real_time_pet_finder.crud_service.entities.GPS;
import com.real_time_pet_finder.crud_service.entities.Pet;
import com.real_time_pet_finder.crud_service.entities.enums.Roles;

import java.util.List;
import java.util.UUID;

public record ClientResponseDTO(
        long id,
        String name,
        String email,
        String surname,
        String code,
        Roles role

) {
}
