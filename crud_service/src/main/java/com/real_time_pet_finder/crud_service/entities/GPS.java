package com.real_time_pet_finder.crud_service.entities;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "devices")
public class GPS {
    @Id
    private long id;
    @NotNull
    @Column("pet_id")
    private long petId;
    @Column("client_id")
    @NotNull
    private long clientId;
    @NotBlank
    private String model;
    @NotBlank
    private UUID uuid;
}
