package com.real_time_pet_finder.crud_service.entities.DTOs.client;

import com.real_time_pet_finder.crud_service.entities.DTOs.pet.PetDTO;
import com.real_time_pet_finder.crud_service.entities.enums.Roles;

import java.util.List;

public record ClientDTOResponseWithPets(
        long id,
        String name,
        String email,
        String surname,
        String code,
        Roles role,
        List<PetDTO> petList
) {

}
