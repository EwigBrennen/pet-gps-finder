package com.real_time_pet_finder.crud_service.util;

import com.real_time_pet_finder.crud_service.entities.Client;
import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientDTOResponseWithPets;
import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientRequestDTO;
import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientResponseDTO;
import com.real_time_pet_finder.crud_service.entities.DTOs.gps.GPSResponseDTO;
import com.real_time_pet_finder.crud_service.entities.DTOs.pet.PetDTO;
import com.real_time_pet_finder.crud_service.entities.GPS;
import com.real_time_pet_finder.crud_service.entities.Pet;

public class Entity2DtoConverter {
    public static ClientRequestDTO mapClientRequestToDTO(Client client){
        return new ClientRequestDTO(client.getId(),client.getName(),client.getEmail(),client.getPassword(),client.getSurname(), client.getCode(),client.getRole());
    }
    public static Client mapClientRequestDTOToClient(ClientRequestDTO dto){
        return new Client(dto.id(),dto.name(), dto.surname(), dto.email(), dto.password(), dto.code(), dto.role());
    }


    public static ClientResponseDTO mapClientResponseToDTO(Client client){
        return  new ClientResponseDTO(
                client.getId(),client.getName(),client.getEmail(),client.getSurname(), client.getCode(),client.getRole()
        );
    }

    public static GPSResponseDTO mapGPS2GPSResponseDTO(GPS gps){
        return new GPSResponseDTO(
                gps.getId(), gps.getPetId(), gps.getClientId(), gps.getModel(),gps.getUuid()
        );
    }
    public static GPS mapDTO2GPs(GPSResponseDTO dto){
        return new GPS(
                dto.id(),dto.petId(),dto.clientId(),dto.model(),dto.uuid()
        );
    }

    public static Pet mapDTO2Pet(PetDTO dto){
        return new Pet(
                dto.id(),
                dto.species(),
                dto.clientId(),
                dto.deviceId()

        );
    }
    public static PetDTO mapPet2DTO(Pet pet){
        return new PetDTO(
                pet.getId(),
                pet.getSpecies(),
                pet.getClientId(),
                pet.getDeviceId()

        );
    }


}
