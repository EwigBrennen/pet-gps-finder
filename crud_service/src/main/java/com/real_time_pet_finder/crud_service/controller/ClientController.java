package com.real_time_pet_finder.crud_service.controller;

import com.real_time_pet_finder.crud_service.entities.Client;
import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientDTOResponseWithPets;
import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientRequestDTO;
import com.real_time_pet_finder.crud_service.entities.DTOs.client.ClientResponseDTO;
import com.real_time_pet_finder.crud_service.services.ClientServiceImp;
import com.real_time_pet_finder.crud_service.services.PetServiceImp;
import com.real_time_pet_finder.crud_service.util.Entity2DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/users/clients")
public class ClientController {
    @Autowired
    ClientServiceImp clientServiceImp;
    @Autowired
    PetServiceImp petServiceImp;
    @GetMapping("/")
    public Flux<ClientResponseDTO> getClients(){
        return clientServiceImp
                .getAll()
                .map(Entity2DtoConverter::mapClientResponseToDTO)
                .switchIfEmpty(Mono.error(new NotFoundException("Clients with the given parameter doesn't exist")));
    }
    @GetMapping("/{id}")
    public Mono<ClientResponseDTO> getSingleClient(@PathVariable long id){
        return clientServiceImp.
                getById(id)
                .map(Entity2DtoConverter::mapClientResponseToDTO)
                .switchIfEmpty(Mono.error(new NotFoundException("Client with the given parameter doesn't exist")));
    }
    @GetMapping("/client/pets/{id}")
    public Mono<ClientDTOResponseWithPets> getClientWithPets(@PathVariable long id){
        return clientServiceImp.getClientithPets(id)
                .switchIfEmpty(Mono.error(new NotFoundException("Client with the given parameter doesn't exist")));
    }
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin
    public Mono<ClientRequestDTO> createClient(@RequestBody ClientRequestDTO dto){
        return clientServiceImp.create(Entity2DtoConverter.mapClientRequestDTOToClient(dto))
                .onErrorMap(ex -> new RuntimeException("Error creating resource: "+ex.getMessage()))
                .map(Entity2DtoConverter::mapClientRequestToDTO);

    }
    @PutMapping("/edit/{id}")
    public Mono<ClientRequestDTO> updateClient(@PathVariable long id,@RequestBody ClientRequestDTO dto){
        return clientServiceImp
                .update(id,Entity2DtoConverter.mapClientRequestDTOToClient(dto))
                .switchIfEmpty(Mono.error(new NotFoundException("Client with the given parameter doesn't exist")))
                .onErrorMap(ex -> new RuntimeException("Error creating resource: "+ex.getMessage()))
                .map(Entity2DtoConverter::mapClientRequestToDTO);
    }
    @DeleteMapping("/delete/{id}")
    public Mono<?> deleteClient(@PathVariable long id){
        return clientServiceImp.getById(id)
                .flatMap(existingClient -> clientServiceImp.removeById(id)
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .switchIfEmpty(Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT)));
    }



    @ResponseStatus(HttpStatus.NOT_FOUND)
    static class NotFoundException extends RuntimeException {
        public NotFoundException(String message) {
            super(message);
        }
    }
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private String handleNotFoundException(NotFoundException ex) {
        return ex.getMessage();
    }

}
