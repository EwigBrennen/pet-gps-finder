package com.real_time_pet_finder.crud_service.controller;
import com.real_time_pet_finder.crud_service.entities.DTOs.pet.PetDTO;
import com.real_time_pet_finder.crud_service.services.PetServiceImp;
import com.real_time_pet_finder.crud_service.util.Entity2DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/users/pets")
@CrossOrigin
public class PetController {
    @Autowired
    PetServiceImp petServiceImp;

    @GetMapping("/")
    public Flux<PetDTO> getAllPets(){
        return petServiceImp.getAll()
                .map(Entity2DtoConverter::mapPet2DTO)
                .switchIfEmpty(Mono.error(new ClientController.NotFoundException("Clients with the given parameter doesn't exist")));

    }

    @GetMapping("/{id}")
    public Mono<PetDTO> getSinglePetById(@PathVariable long id){
        return petServiceImp
                .getById(id)
                .map(Entity2DtoConverter::mapPet2DTO)
                .switchIfEmpty(Mono.error(new ClientController.NotFoundException("Client with the given parameter doesn't exist")));

    }
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<PetDTO> createPet(@RequestBody PetDTO dto){
        return petServiceImp.create(Entity2DtoConverter.mapDTO2Pet(dto))
                .onErrorMap(ex -> new RuntimeException("Error creating resource: "+ex.getMessage()))
                .map(Entity2DtoConverter::mapPet2DTO);
    }

    @PutMapping("/edit/{id}")
    public Mono<PetDTO> updatePet(@PathVariable long id, @RequestBody PetDTO dto){
        return petServiceImp
                .update(id,Entity2DtoConverter.mapDTO2Pet(dto))
                .switchIfEmpty(Mono.error(new ClientController.NotFoundException("Client with the given parameter doesn't exist")))
                .onErrorMap(ex -> new RuntimeException("Error creating resource: "+ex.getMessage()))
                .map(Entity2DtoConverter::mapPet2DTO);
    }
    @DeleteMapping("/delete/{id}")
    public Mono<?> deletePet(@PathVariable long id){
        return petServiceImp.getById(id)
                .flatMap(existingPet -> petServiceImp.removeById(existingPet.getId())
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .switchIfEmpty(Mono.just(new ResponseEntity<>(HttpStatus.NOT_FOUND)));
    }

    @ExceptionHandler(ClientController.NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private String handleNotFoundException(ClientController.NotFoundException ex) {
        return ex.getMessage();
    }



}
