package com.real_time_pet_finder.crud_service.controller;

import com.real_time_pet_finder.crud_service.entities.DTOs.gps.GPSResponseDTO;
import com.real_time_pet_finder.crud_service.entities.GPS;
import com.real_time_pet_finder.crud_service.services.GPSServiceImp;
import com.real_time_pet_finder.crud_service.util.Entity2DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/users/gps")
@CrossOrigin
public class GPSController {

    @Autowired
    GPSServiceImp gpsServiceImp;
    @GetMapping("/")
    public Flux<GPSResponseDTO> getAllDevices (){
        return gpsServiceImp
                .getAll()
                .map(Entity2DtoConverter::mapGPS2GPSResponseDTO)
                .switchIfEmpty(Mono.error(new ClientController.NotFoundException("Event with the given parameter doesn't exist")));
    }
    @GetMapping("/byClient")
    public Flux<GPSResponseDTO> getAllDevicesByClientId(@RequestParam long clientId){
        return gpsServiceImp
                .getAllDevicesByClientId(clientId)
                .map(Entity2DtoConverter::mapGPS2GPSResponseDTO)
                .switchIfEmpty(Mono.error(new ClientController.NotFoundException("Event with the given parameter doesn't exist")));

    }


    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<GPSResponseDTO> saveDevice(@RequestBody GPSResponseDTO dto){
        return gpsServiceImp.create(Entity2DtoConverter.mapDTO2GPs(dto))
                .map(Entity2DtoConverter::mapGPS2GPSResponseDTO)
                .switchIfEmpty(Mono.error(new ClientController.NotFoundException("Event with the given parameter doesn't exist")));
    }

    @PutMapping("/edit/{id}")
    public Mono<GPSResponseDTO>updateDevice(@PathVariable long id, @RequestBody GPSResponseDTO dto){
        return gpsServiceImp
                .update(id, Entity2DtoConverter.mapDTO2GPs(dto))
                .switchIfEmpty(Mono.error(new ClientController.NotFoundException("Client with the given parameter doesn't exist")))
                .onErrorMap(ex -> new RuntimeException("Error creating resource: "+ex.getMessage()))
                .map(Entity2DtoConverter::mapGPS2GPSResponseDTO);

    }
    @DeleteMapping("/delete/{id}")
    public Mono<?> deleteClient(@PathVariable long id){
        return gpsServiceImp.getById(id)
                .flatMap(existingDevice -> gpsServiceImp.removeById(existingDevice.getId())
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .switchIfEmpty(Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT)));

    }

    @ExceptionHandler(ClientController.NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private String handleNotFoundException(ClientController.NotFoundException ex) {
        return ex.getMessage();
    }


}
