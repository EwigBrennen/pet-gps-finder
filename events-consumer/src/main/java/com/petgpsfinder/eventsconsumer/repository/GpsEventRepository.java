package com.petgpsfinder.eventsconsumer.repository;

import com.petgpsfinder.eventsconsumer.Entity.GpsEvent;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GpsEventRepository extends ReactiveMongoRepository<GpsEvent, String> {
}
