package com.petgpsfinder.eventsconsumer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.petgpsfinder.eventsconsumer.Entity.GpsEvent;
import com.petgpsfinder.eventsconsumer.Entity.GpsEventDto;
import com.petgpsfinder.eventsconsumer.repository.GpsEventRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
public class QConsumer {
    @Autowired
    GpsEventRepository gpsEventRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @RabbitListener(queues = "q.gps")
    public void getMessage(String message) throws JsonProcessingException {
        var gpsEventDto = objectMapper.readValue(message, GpsEventDto.class);
        var gpsEntity = new GpsEvent(
                gpsEventDto.latitude(),
                gpsEventDto.longitude(),
                gpsEventDto.altitude(),
                gpsEventDto.serial()
        );
        gpsEntity.setLocalDate(LocalDate.now());
        createEvent(gpsEntity).subscribe(System.out::println);


    }
    private Mono<GpsEvent> createEvent(GpsEvent gpsEvent){
        return gpsEventRepository.insert(gpsEvent);
    }
}
