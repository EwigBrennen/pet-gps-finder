package com.petgpsfinder.eventsconsumer.Entity;

public record GpsEventDto(
        String latitude,
        String longitude,
        String altitude,
        String serial
) {
}
