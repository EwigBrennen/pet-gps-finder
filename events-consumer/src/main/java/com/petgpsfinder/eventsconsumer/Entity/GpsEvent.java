package com.petgpsfinder.eventsconsumer.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
@Document(collation = "events")
public class GpsEvent {
    @Id
    private String id;
    private String latitude;
    private String longitude;
    private String altitude;
    @JsonProperty("birth_date")
    @JsonFormat(pattern = "dd-mm-yyyy")
    private LocalDate localDate;
    private String serial;
    public GpsEvent(){
    }

    public GpsEvent(String latitude, String longitude, String altitude, String serial) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.serial = serial;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }



    @Override
    public String toString() {
        return "GpsEvent {" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", altitude='" + altitude + '\'' +
                ", localDate=" + localDate +
                ", serial='" + serial + '\'' +
                '}';
    }
}
