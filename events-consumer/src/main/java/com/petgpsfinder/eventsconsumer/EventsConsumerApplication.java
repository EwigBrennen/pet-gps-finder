package com.petgpsfinder.eventsconsumer;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableRabbit
public class EventsConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventsConsumerApplication.class, args);
	}

}
