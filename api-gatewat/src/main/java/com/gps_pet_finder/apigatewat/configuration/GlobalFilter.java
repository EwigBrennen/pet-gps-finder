package com.gps_pet_finder.apigatewat.configuration;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public class GlobalFilter implements org.springframework.cloud.gateway.filter.GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return null;
    }

    public RouteLocator routeLocator(RouteLocatorBuilder builder){
        return builder.routes()
                .route(r -> r.path("/users/users-service/v3/api-docs")
                        .and().method(HttpMethod.GET).uri("lb://main-crud-service"))
                .route(r -> r.path("/events/events-service/v3/api-docs")
                        .and().method(HttpMethod.GET).uri("lb://crud-events-service"))
                .build();
    }
}
